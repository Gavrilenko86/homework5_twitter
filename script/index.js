const usersURL = "https://ajax.test-danit.com/api/json/users";
const postsURL = "https://ajax.test-danit.com/api/json/posts";

class Required {
    get(url) {
        return fetch(url, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            },
        }).then((response) => response.json());
    }
    delete(url) {
        return fetch(url, {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
            },
        }).then((response) => response);
    }
    post(url, postObj) {
        return fetch(url, {
            method: "POST",
            body: JSON.stringify(postObj),
            headers: {
                "Content-Type": "application/json",
            },
        }).then((response) => response.json());
    }
    put(url, postObj) {
        return fetch(url, {
            method: "PUT",
            body: JSON.stringify(postObj),
            headers: {
                "Content-Type": "application/json",
            },
        }).then((response) => response.json());
    }
}

class Card {
    render(post, author, postUrl = "") {
        const card = document.createElement("div");
        card.classList.add("card");
        const changePost = document.createElement("button");
        changePost.classList.add("card-change");
        changePost.innerHTML =
            '<svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" fill="none" viewBox="0 0 16 16" id="document-edit"><path fill="#212121" d="M8 1V4.5C8 5.32843 8.67157 6 9.5 6H13V7.04196C12.2308 6.8982 11.4062 7.12385 10.8112 7.71893L7.05493 11.4752C6.70776 11.8223 6.46148 12.2573 6.3424 12.7336L6.05114 13.8987C5.95294 14.2915 5.99867 14.6726 6.14643 15H4.5C3.67157 15 3 14.3284 3 13.5V2.5C3 1.67157 3.67157 1 4.5 1H8Z"></path><path fill="#212121" d="M9 1.25V4.5C9 4.77614 9.22386 5 9.5 5H12.75L9 1.25zM11.5183 8.42604C12.0863 7.85799 13.0073 7.85799 13.5754 8.42604 14.1434 8.99409 14.1434 9.91508 13.5754 10.4831L9.81912 14.2394C9.60012 14.4584 9.32571 14.6137 9.02524 14.6889L7.8602 14.9801C7.35355 15.1068 6.89462 14.6478 7.02128 14.1412L7.31254 12.9762C7.38766 12.6757 7.54303 12.4013 7.76203 12.1823L11.5183 8.42604zM12.2254 9.13314L8.46914 12.8894C8.37829 12.9802 8.31385 13.0941 8.28269 13.2187L8.11601 13.8854 8.78271 13.7187C8.90734 13.6875 9.02117 13.6231 9.11202 13.5323L12.8683 9.77602C13.0458 9.5985 13.0458 9.31067 12.8683 9.13314 12.6907 8.95562 12.4029 8.95562 12.2254 9.13314z"></path></svg>';
            

        changePost.addEventListener("click", (e) => {
            e.preventDefault();
            const card = e.target.closest(".card");
            const modal = new Modal();
            document.body.append(
                modal.render(
                    author.name,
                    author.email,
                    post.title,
                    post.body,
                    modal.changePostHandler,
                    postUrl,
                    document.body.main,
                    post,
                    card
                )
            );
        });

        const deletePost = document.createElement("button");
        deletePost.classList.add("card-delete");
        deletePost.innerHTML =
            '<svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" data-name="Layer 1" viewBox="0 0 98 98" id="delete"><g data-name="<Group>"><path d="M30.9,78.9A5.7,5.7,0,0,0,36.8,84H62a5.8,5.8,0,0,0,6-5.2L72.1,36H25.9Z" data-name="<Path>"></path><path d="M79.6,23H61V19.8A5.6,5.6,0,0,0,55.3,14H42.7A5.6,5.6,0,0,0,37,19.8V23H18.4a2,2,0,0,0,0,4H79.6a2,2,0,0,0,0-4ZM57,23H41V19.8A1.6,1.6,0,0,1,42.7,18H55.3A1.6,1.6,0,0,1,57,19.8Z" data-name="<Compound Path>"></path></g></svg>';

        deletePost.addEventListener("click", () => {
            if (post.id > 100) {
                card.remove();
                return;
            }
            const required = new Required();
            required
                .delete(postUrl + `/${post.id}`)
                .then((response) => {
                    if (response.ok === true && response.status === 200) {
                        card.remove();
                    }
                })
                .catch((error) => {
                    console.error(error.message);
                });
        });

        const cardAuthor = document.createElement("h2");
        cardAuthor.classList.add("card-author");

        const cardAuthorEmail = document.createElement("p");
        cardAuthorEmail.classList.add("card-email");

        const cardTitle = document.createElement("p");
        cardTitle.classList.add("card-title");

        const cardText = document.createElement("p");
        cardText.classList.add("card-text");

        cardAuthor.textContent = author.name;
        cardAuthorEmail.textContent = author.email;
        cardTitle.textContent = post.title;
        cardText.textContent = post.body;

        card.append(
            changePost,
            deletePost,
            cardAuthor,
            cardAuthorEmail,
            cardTitle,
            cardText
        );

        return card;
    }
}

class Post {
    getPosts(url) {
        const required = new Required();
        return required.get(url);
    }
    getUsers(url) {
        const required = new Required();
        return required.get(url);
    }
    createPosts(postUrl, usersUrl) {
        const animation = document.querySelector(".holder");

        const postElements = document.createElement("div");

        this.getPosts(postUrl)
            .then((posts) => {
                this.getUsers(usersUrl)
                    .then((users) => {
                        animation.style.display = "none";

                        posts.forEach((post) => {
                            const author = users.find((user) => user.id === post.userId);
                            const card = new Card();
                            postElements.append(card.render(post, author, postUrl));
                        });
                    })
                    .catch((error) => {
                        console.error(error.message);
                    });
            })
            .catch((error) => {
                console.error(error.message);
            });
        return postElements;
    }
}

class Modal {
    render(
        authorInput = "",
        emailInput = "",
        titleInput = "",
        contentInput = "",
        handler,
        postUrl = "",
        root = document.body.main,
        post,
        card
    ) {
        const modal = document.createElement("div");
        modal.classList.add("modal");

        const form = document.createElement("form");
        form.classList.add("modal-form");
        const author = document.createElement("input");
        author.placeholder = "Author name";
        author.value = authorInput;
        const email = document.createElement("input");
        email.value = emailInput;
        email.placeholder = "Author email";
        const postTitle = document.createElement("input");
        postTitle.value = titleInput;
        postTitle.placeholder = "Post title";
        const postText = document.createElement("textarea");
        postText.value = contentInput;
        postText.placeholder = "Post content";
        const postCreate = document.createElement("button");
        if (contentInput === "") {
            postCreate.textContent = "Create post";
        } else {
            postCreate.textContent = "Change post";
        }
        postCreate.addEventListener("click", (e) => {
            e.preventDefault();
            handler(author, email, postTitle, postText, postUrl, root, post, card);
            modal.remove();
        });
        form.append(author, email, postTitle, postText, postCreate);
        modal.append(form);
        return modal;
    }
    createPostHandler(author, email, title, content, url, root, post, card) {
        const required = new Required();
        required
            .post(url, { body: content.value, title: title.value, userId: 1 })
            .then((response) => {
                const card = new Card();
                root.prepend(
                    card.render(response, { name: author.value, email: email.value }, url)
                );
            })
            .catch((error) => {
                console.error(error.message);
            });
    }
    changePostHandler(author, email, title, content, url, root, post, card) {
        const required = new Required();
        required
            .put(`${url}/${post.id}`, {
                body: content.value,
                title: title.value,
                userId: 1,
            })
            .then((response) => {
                card.querySelector(".card-author").textContent = author.value;
                card.querySelector(".card-email").textContent = email.value;
                card.querySelector(".card-title").textContent = title.value;
                card.querySelector(".card-text").textContent = content.value;
            })
            .catch((error) => {
                if (post.id > 100) {
                    card.querySelector(".card-author").textContent = author.value;
                    card.querySelector(".card-email").textContent = email.value;
                    card.querySelector(".card-title").textContent = title.value;
                    card.querySelector(".card-text").textContent = content.value;
                } else {
                    console.error(error.message);
                }
            });
    }
}

const posts = new Post();
const root = document.querySelector("#root");

const createNewPost = document.createElement("button");
createNewPost.textContent = "Create post";
createNewPost.addEventListener("click", () => {
    const modal = new Modal();
    document.body.append(
        modal.render("", "", "", "", modal.createPostHandler, postsURL, root)
    );
});

document.body.prepend(createNewPost);
root.append(posts.createPosts(postsURL, usersURL));
